import sqlite3
from bantuan import mendapatkan_project_directory
from typing import Callable
from sqlite3 import Connection

def query_database_with_commit(query: Callable[[Connection], None]):
    conn = database()
    result = query(conn)
    conn.commit()
    conn.close()
    return result
def query_database(query: Callable[[Connection], None]):
    conn = database()
    result = query(conn)
    conn.close()
    return result

def database():
    project_dir = mendapatkan_project_directory()
    conn = sqlite3.connect(project_dir / 'db' / 'database.db')
    conn.row_factory = sqlite3.Row
    return conn