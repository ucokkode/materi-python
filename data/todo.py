from dataclasses import dataclass

@dataclass(frozen=True)
class Todo:
       id: int | None 
       todo: str | None