from flask import Flask, request, render_template, jsonify
from data.todo import Todo
from penyimpanan.koneksi import query_database, query_database_with_commit

app = Flask(__name__)


def insertTodo(todo: Todo):
    print(f"Insert todo: {todo}")
    insert = lambda conn: conn.execute("INSERT INTO todos (todo) VALUES (?);", (todo.todo,))
    return query_database_with_commit(insert)

def getTodos():
    select = query_database(lambda conn: conn.execute('SELECT * FROM todos').fetchall())
    todos = [Todo(id=todo["id"], todo=todo["todo"]) for todo in select ] or []
    return todos

def deleteTodos(todo: Todo):
    delete = lambda conn: conn.execute('DELETE FROM todos WHERE id = ?', (todo.id, ))
    return query_database_with_commit(delete)


@app.route("/hapus", methods=["POST"])
def hapus():
    json = request.json
    data = Todo(id=json["id"], todo=None)
    deleteTodos(data)
    todos =  getTodos()
    return render_template("todo-list.html", todos=todos)

@app.route("/tambah", methods=["POST"])
def tambah():
    json = request.json
    data = Todo(id=None, todo=json["todo"])
    insertTodo(data)
    todos =  getTodos()
    return render_template("todo-list.html", todos=todos)

@app.route("/")
def index():
    todos =  getTodos()
    return render_template("index.html", todos=todos) 


if __name__ == "__main__":
    app.jinja_env.auto_reload=True
    app.config["TEMPLATES_AUTO_RELOAD"] = True
