const form = document.getElementById("form-tambah");
if (form != null) {
       form.addEventListener("submit", formPenambahan);
}

function sendData(method, url, data, callback) {
       fetch(url, {
              method: method,
              headers: {
                     "Content-Type": "application/json"
              },
              body: JSON.stringify(data)
       })
              .then(response => response.text())
              .then(text => {
                     callback(text)
              })
              .catch(error => {
                     console.error("Error:", error);
              });

}


function hapus(id) {
       let data = { id: id };
       sendData("post", "/hapus", data, function (text) {
              const todoList = document.getElementById("todo-list");
              todoList.innerHTML = text
       })
}

function formPenambahan(event) {
       event.preventDefault();
       const data = new FormData(event.target)
       const dict_data = Object.fromEntries(data.entries())
       sendData(form.method, form.action, dict_data, function (text) {
              const todoList = document.getElementById("todo-list");
              todoList.innerHTML = text
       })
       event.target.reset();
}
