import sqlite3
from bantuan import mendapatkan_project_directory

project_dir = mendapatkan_project_directory()
connection = sqlite3.connect(project_dir / "db/database.db")
with open(project_dir / "db" / "schema.sql") as file:
       skema_sql = file.read()
       connection.executescript(skema_sql)

cur = connection.cursor()

cur.execute("INSERT INTO todos (todo) VALUES (?);",('Belajar python',))

connection.commit()
connection.close()