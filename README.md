TodoList 
===============
### Stay on Top of Your Schedule with our Todo List App




![Review](./assets/review.gif)






Tujuan repository ini adalah untuk pembelajaran


untuk menjalankan di butuhkan:
- Python 
- Poetry ( Python Package Manager ) 


install poetry telebih dahulu di [sini](https://python-poetry.org/docs/)

Keterangan langsung: 
- untuk **Linux**:
```bash
curl -sSL https://install.python-poetry.org | python3 -
```
- untuk **Windows**:
```powershell
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

Sehabis itu install paket paket yang di perlukan untuk menjalankan aplikasi ini dengan poetry
```
poetry install
```

Sehabis itu untuk menjalankan:
```
poetry run flask run --debug
```